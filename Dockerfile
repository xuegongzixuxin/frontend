FROM nginx:1.23.4-alpine-slim

MAINTAINER 545923664@qq.com

#用dist命名老报错 mv dist dist2后不报错
ADD pc/ /usr/share/nginx/html

COPY docker/nginx.conf /etc/nginx/nginx.conf